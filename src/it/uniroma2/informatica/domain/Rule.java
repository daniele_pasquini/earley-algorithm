package it.uniroma2.informatica.domain;

import java.util.List;

/**
 * Created by Giovanni Lorenzo Napoleoni on 27/11/2014.
 */
public abstract class Rule {

    private String mLeftHandSide;
    private List<String> mRightHandSide;

    /***************************************************************************************************************\
                                                CONSTRUCTOR
    \***************************************************************************************************************/

    public Rule() {

    }

    /***************************************************************************************************************\
                                               ABSTRACT METHODS
    \***************************************************************************************************************/

    public abstract String getNextSymbol();

    /***************************************************************************************************************\
                                                   METHODS
    \***************************************************************************************************************/


    /***************************************************************************************************************\
                                               GETTER AND SETTER
    \***************************************************************************************************************/

    public String getLeftHandSide() {
        return mLeftHandSide;
    }

    public void setLeftHandSide(String leftHandSide) {
        this.mLeftHandSide = leftHandSide;
    }

    public List<String> getRightHandSide() {
        return mRightHandSide;
    }

    public void setRightHandSide(List<String> rightHandSide) {
        this.mRightHandSide = rightHandSide;
    }
}
