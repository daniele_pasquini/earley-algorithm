package it.uniroma2.informatica.domain;

/**
 * Created by Lorenzo on 27/11/2014.
 */
public class DottedRule extends Rule {

    private int mDotPosition;

    /***************************************************************************************************************\
                                                  CONSTRUCTOR
    \***************************************************************************************************************/

    public DottedRule() {
        super();
    }


    /***************************************************************************************************************\
                                                    METHODS
    \***************************************************************************************************************/

    public void forward() {

    }

    public boolean isActive() {
        return true;
    }

    public String getNextSymbol() {
        return null;
    }

    /***************************************************************************************************************\
                                                GETTER AND SETTER
    \***************************************************************************************************************/

    public int getDotPosition() {
        return mDotPosition;
    }

    public void setDotPosition(int dotPosition) {
        this.mDotPosition = dotPosition;
    }

}
