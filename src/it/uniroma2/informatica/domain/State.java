package it.uniroma2.informatica.domain;

/**
 * Created by Giovanni Lorenzo Napoleoni on 27/11/2014.
 */
public class State {

    private DottedRule mDottedRule;
    private int mLeftBracket;
    private int mRightBracket;

    /***************************************************************************************************************\
                                                CONSTRUCTOR
    \***************************************************************************************************************/

    public State() {
    }

    /***************************************************************************************************************\
                                               GETTER AND SETTER
    \***************************************************************************************************************/


    //TODO In questo metodo è necessario effettuare la clonazione della DottedRule
    public DottedRule getDottedRule() {
        return null;
    }

    public void setDottedRule(DottedRule dottedRule) {
        this.mDottedRule = dottedRule;
    }

    public int getLeftBracket() {
        return mLeftBracket;
    }

    public void setLeftBracket(int leftBracket) {
        this.mLeftBracket = leftBracket;
    }

    public int getRightBracket() {
        return mRightBracket;
    }

    public void setRightBracket(int rightBracket) {
        this.mRightBracket = rightBracket;
    }
}
