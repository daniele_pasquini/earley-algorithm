package it.uniroma2.informatica.domain;

import java.util.List;

/**
 * Created by Giovanni Lorenzo Napoleoni on 27/11/2014.
 */
public class Grammar {

    private List<Rule> mRulesOfGrammar;

    public List<Rule> getRulesOfGrammar() {
        return mRulesOfGrammar;
    }

    public void setRulesOfGrammar(List<Rule> rulesOfGrammar) {
        this.mRulesOfGrammar = rulesOfGrammar;
    }
}
