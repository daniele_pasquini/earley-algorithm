package it.uniroma2.informatica.parser;

import it.uniroma2.informatica.domain.Chart;
import it.uniroma2.informatica.domain.DottedRule;
import it.uniroma2.informatica.domain.Grammar;
import it.uniroma2.informatica.domain.Word;

import java.util.ArrayList;

/**
 * Created by Giovanni Lorenzo Napoleoni on 27/11/2014.
 */
public class EarlyParse {

    //Frase in input
    private ArrayList<Word> mWords;
    //Grammatica
    private Grammar mGrammar;

    public EarlyParse(ArrayList<Word> mWords, Grammar grammar) {
        this.mWords = mWords;
        this.mGrammar = grammar;
    }

    public Chart parse(){

        return null;
    }


    private void predictor(DottedRule rule, int i, int j){

    }

    private void scanner(DottedRule rule, int i, int j){

    }

    private void completer(DottedRule rule, int i, int j){

    }

    public ArrayList<Word> getWords() {
        return mWords;
    }

    public void setWords(ArrayList<Word> words) {
        this.mWords = words;
    }

    public Grammar getGrammar() {
        return mGrammar;
    }

    public void setGrammar(Grammar grammar) {
        this.mGrammar = grammar;
    }


}
